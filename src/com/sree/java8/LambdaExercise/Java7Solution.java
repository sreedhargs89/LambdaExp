package com.sree.java8.LambdaExercise;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Java7Solution {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(new Person("Sree", "GS", 29), new Person("Swamy", "Vivekananada", 35),
				new Person("Lewis", "Chandra", 50), new Person("Keshavananda", "Bharathi", 60),
				new Person("Sri", "Krishna", 45));

		// Step1: Sort list by last name
		// Instead of creating new condition override class, we can do it in hidden class
		// This condition implementation of the test() method also there, which does
		// actual check
		Collections.sort(people, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getLastName().compareTo(o2.getLastName());
			}
		});

		// Step2: Create a method that prints all elements in list
		System.out.println("Printing All People");
		printAll(people);

		// Step3: Create a method prints all the people that have last name beginning with C
		System.out.println("Printing Person with last name starts with \"C\"");
		printLastNameBeginWith(people, new Condition() {
			@Override
			public boolean test(Person p) {
				return p.getLastName().startsWith("C");
			}
		});
	}

	// Condition interface has been created with the test method - this method
	// returns the conditional test results

	private static void printLastNameBeginWith(List<Person> people, Condition condition) {
		for (Person p : people) {
			if (condition.test(p))
				System.out.println(p);
		}
	}

	private static void printAll(List<Person> people) {
		for (Person p : people)
			System.out.println(p);
	}
}

	interface Condition {
		public boolean test(Person p);
	}