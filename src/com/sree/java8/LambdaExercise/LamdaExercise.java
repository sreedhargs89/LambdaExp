package com.sree.java8.LambdaExercise;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LamdaExercise {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(new Person("Sree", "GS", 29), new Person("Swamy", "Vivekananada", 35),
				new Person("Lewis", "Chandra", 50), new Person("Keshavananda", "Bharathi", 60),
				new Person("Sri", "Krishna", 45));

		// Step1: Sort list by last name
		// Lambda expression -- It takes the p1 and p2 and check the last name -- order and returns -- What comparator doing replaced with the lambda function
		Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));

		// Step2: Create a method that prints all elements in list
		System.out.println("Printing All People");
		// Here condtion is always true
		printConditionally(people, p -> true);

		// Step3: Create a method prints all the people that have last name beginning with C
		System.out.println("Printing Person with last name starts with \"C\"");
		printConditionally(people, (p)->p.getLastName().startsWith("C"));		
		
		// Step4: Create a method prints all the people name starts with "S"
		System.out.println("Printing Person with last name starts with \"S\"");
		printConditionally(people, (p)->p.getFirstName().startsWith("S"));		
				
	}

	// Condition interface has been created with the test method - this method
	// returns the conditional test results

	private static void printConditionally(List<Person> people, Condition condition) {
		for (Person p : people) {
			if (condition.test(p))
				System.out.println(p);
		}
	}

}



