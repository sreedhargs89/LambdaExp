package com.sree.java8.lambda;

public class Geeter {

	public void greet(Greeting greeting) {
		System.out.println("Hello World..!");
	}
	
	public static void main(String[] args) {
		HelloWorldGreeting hGreeting = new HelloWorldGreeting();
		hGreeting.perform();
		
		// Here we are passing the Thing that has the behavior
		// can we do this without sending objects
		// sending values / Function as values
		Greeting myLambdaFunction = ()->System.out.println("Hello World");
		
		// Inline function, without creating any class has worked
		myLambdaFunction.perform();
		
		AddLamda addFunction = (int a, int b) ->  a+b;
	}
}

interface AddLamda{
	int add(int x, int y);
}