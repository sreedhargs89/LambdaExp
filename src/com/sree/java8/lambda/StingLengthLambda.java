package com.sree.java8.lambda;

public class StingLengthLambda {

	public static void main(String[] args) {
		
//		StringLength slength = (String s) -> s.length();
		StringLength slength = s -> s.length();
		
		System.out.println(slength.length("Sree"));
		
		
		
	}
	
	interface StringLength{
		int length(String s);
	}
}
