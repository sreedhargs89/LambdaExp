package com.sree.java8.lambda;

public class ThreaingLamda {

	
	public static void main(String[] args) {
/*		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("Running Thread");
			}
		});
*/
		//one way
		//Runnable runthread = () -> System.out.println("Thread running..!");
		//Thread thread = new Thread(runthread);
		
		Thread thread = new Thread(()->System.out.println("Thread Running using Lambda"));
		thread.run();

	}
}

